Illusion
===================

### What is Illusion?

* Illusion is a set of abstractions forming a general purpose micro framework, kick-starting development of CLI, API & GUI web apps.

* Illusion apps usually are long running processes. Unless you need to serve static content or deal with client certs or ssl offload, you don't even need a web server in front.
 
 * Illusion is being used in production, handling thousands of messages per second (in parallel over multiple cores).
 
* Illusion takes some use of the very good [reactphp](https://github.com/reactphp) project for socket stuff.
 
* Illusion is a modern PHP framework.
 
* Illusion loves convention over configuration.

* If you don't like frameworks and the "Don't call us, we'll call you" principle, many Illusion features can just as easily be used as a library using composer.


### Out of the box

* Actions
  * Callable resources identifiers
* Reactive Networking
  * Built-in protocols
  * Protocol builder
* Database abstraction
  * Intelligent SQL builder
  * Multiple SQL flavours
* Rule based database access
  * Intelligent CRUD commands
  * Write simple rule logic
* Users, groups and sessions
  * Group based access control
  * Cross connection sessions
  * Signed data and control messages
  * Revokable access keys
  * Temporary access tokens
* Process Management
* Package Management
* Hackable templating

### Installation

Illusion installs as a self-contained project using `composer` for dependencies.
The installer adds a one-liner to your `.bash_profile` to enable easy `cli` use.

Install using one of the following methods.

* Run the bash installer at `illusion.jonashall.in`
* Use composer
* Illusion boilerplate:
  1. git clone git@bitbucket.org:pingpal/illusion-boilerplate-v0.2.git illusion
  2. cd illusion
  3. php install.php
  4. illusion add git@bitbucket.org:vendor/project.git
  
### Deploy

1. git clone git@bitbucket.org:pingpal/illusion-boilerplate-v0.2.git deploy
2. cd deploy
3. php composer.phar install
4. php install.php
5. rmgit (find . | grep -v ".gitignore" | grep /.git | xargs rm -rf)
6. deploy self deploy (-i ~/.../cert.pem user@sub.domain.top)
7. deploy self uninstall
8. cd ..
9. rm -rf deploy

## Overview

### Actions

Illusion introduces a concept of actions; strings identifies callable resources.

References to invokable code is called actions. Most often it is a static class method. 

Actions can be used to execute code from arbitrary places. For example API entry points, Command line, or within your app.

Actions is passed to the launch function:

    l('some.action');

Actions are structured as follows.

    Name.Space.Class.method

That action would resolve to this method.

    namespace Name\Space;
   
    class Class {
    	static function method() {}
    }

This could be done a little shorter using aliases.

	/**
	 * # alias user
	 */
    class Class {
    
	    /**
	     * # alias add
	     */
    	static function method() {}
    }

The same method would now be available with simply

    user.add

Methods can also be names alone using the `direct` keyword in place of `alias`.

Actions can also be restricted to per context using `# context cli`. This means that some method has `# access public` it will only be available on the command line. This can be done at class or method level.

### SQL
    This isn't the usuall solve it all ORM. Just a smart builder to let you write complex queries super easy. It does execute them for you using any supported sql backend. (Only MySQL support at this time)

This call chain

    $q = sql()->select()->from('cars')->where('user.id', $id);

Translates to

    SELECT *
    FROM `cars`
        INNER JOIN `user` ON `user`.`id` = `cars`.`user`
    WHERE (`user`.`id` = ?)

This call chain

    $q = sql()->select()->from('cars')->where('user.id', $id);

Translates to

    SELECT *
    FROM `cars`
        INNER JOIN `user` ON `user`.`id` = `cars`.`user`
    WHERE (`user`.`id` = ?)

As you can se, if above pattern is followed, sql() is smart enough to join most queries by its self. If you are unsure, you can always check using $q->toString();

If you change the dot in user.id to a semi colon, the resulting join will be the other way around.

This call chain (note ":")

    $q = sql()->select()->from('cars')->where('user:id', $id);

Translates to

    SELECT *
    FROM `cars`
        INNER JOIN `user` ON `user`.`id` = `cars`.`user`
    WHERE (`user`.`id` = ?)

Get the result

    $result = $q->all();
    $result = $q->row();
    $result = $q->one();

Other examples

    sql()->select()->from('cars')->field('name')->where('id', 1)->all();
    sql()->insert()->into('cars')->set('name', 'betty')->exe();
    sql()->delete()->from('cars')->where('name = ?', $name)->exe();
    sql()->update()->table('cars')->set('name', 'x')->where('id', 1)->exe();

Last insert id and affected rows

The $q->exe() method returns last insert id for insert and affected rows for update and delete operations. You can always access them using $q->lastInsertId() and $q->affectedRows() 

#### Joins

Note the difference between

    echo sql()->select()->from('jonas')->join('one')->join('two')->join('three')->join('four')->toString();
    
    SELECT *
    FROM `jonas`
        INNER JOIN `one` ON `one`.`id` = `jonas`.`one`
        INNER JOIN `two` ON `two`.`id` = `jonas`.`two`
        INNER JOIN `three` ON `three`.`id` = `jonas`.`three`
        INNER JOIN `four` ON `four`.`id` = `jonas`.`four`

and the following

    echo sql()->select()->from('jonas')->join('one.two.three.four')->toString();
    
    SELECT *
    FROM `jonas`
        INNER JOIN `one` ON `one`.`id` = `jonas`.`one`
        INNER JOIN `two` ON `two`.`id` = `one`.`two`
        INNER JOIN `three` ON `three`.`id` = `two`.`three`
        INNER JOIN `four` ON `four`.`id` = `three`.`four`

#### Table definition

    /**
     * # table user
     * # engine InnoDB
     *
     * # column id id
     * # column ok boolean
     * # column name
     */
    
    class User {}

You should always include `# column id id`.

Following column definitions is available as presets.

    id:       int not null primary key auto_increment
    uid:      int unsigned not null unique
    fid:      int not null
    int:      int not null default 0
    uint:     int unsigned not null default 0
    boolean:  tinyint(1) not null default 0
    string:   varchar(255) not null default ''
    unique:   varchar(255) not null unique
    text:     text not null

Each column defaults to `string`

Foreign keys can be set up with

 `# foreign targetTable sourceColumn targetColumn odc`

`odc` is `on delete cascade`.

You can add custom definitions.

`# custom unique(uid)`

Run `sql()->createTableFromName('user');` to set it up.

#### Transactions

The sql() function supports transactions. To ensure correct function with nested transaction, the following pattern will be useful.

    try {
    
        $act = sql()->beginTransaction();
    
        // SQL OPERATIONS GOES HERE
    
        $act->commitTransaction();
    
    } catch (Exception $e) {
    
        @$act && $act->rollbackTransaction();
    
        throw $e;
    }
    
### CRUD

This module/component enables use of CRUD operations for intelligent rule based access to the database. CRUD requests are analysed to provide your rule logic with simple and distinct information that together with session information makes it easy to ensure correct access control. The simple CRUD commands relay and on the power of the SQL builder and allows to you to write short queries that translates to complex SQL queries.

This system allows CRUD queries can be constructed client side and safely be executed server side.

This query

    read(cars).filter(user.id = 1)

Translets to

    SELECT *
    FROM `cars`
        INNER JOIN `user` ON `user`.`id` = `cars`.`user`
    WHERE (`user`.`id` = 1)
    ORDER BY `cars`.`id` DESC
    LIMIT 101

In your table class you can define a method `access` as follows to approve or reject query requests.

    class User {
	    static function access($ctx, $verb, $values, filter) {}
    }

This is what you get decide whether or not to finish:

    {
        "safe": true,
        "values": {
            "user": {
                "id": 1
            }
        },
        "filter": [
            [
                {
                    "field": "user.id",
                    "comparator": "=",
                    "operator": null,
                    "value": 1,
                    "table": "user",
                    "column": "id"
                }
            ]
        ]
    }

Safe is only relevant when there is a filter, more specifically with read and update.

If safe is true, this guarantees three things.

(1) Only tables defined in the context of illusion will be contained in the query
(2) For select/read queries the join chain is consistent with your table definitions
(3) Any given part of the filter will only narrow the result further.

This (3) means, once you have establish that some property of a filter will cause
the result to be within safe bounds, the rest of the filter can remain unknown,
because it will only narrow the result further, thereby stay within safe bounds.

The return value of the access method will decide whether of not the query is allowed to proceed.

A return value of:

FALSE will cause the query not to be accepted.
TRUE means that with respect to the current table, it's all good to proceed.
    (Some other table can still halt the whole thing).
NULL means that this table dont giva a damn. But its not enough to on its own accept the query.
    This alternative is only useful for select/read queries and means that
    this table depends on a joined table to ok the whole thing.

A typical implementation of the access method might look like as follows.

    static function access($ctx, $verb, $safe, $filter, $fields) {
            
        if ($ctx->session()->isMemberOf([ 'super' ])) {
            return true;
        }
		
		if (!$safe && ($verb == 'read' || $verb == 'update')) {
			return false;
		}    

        switch ($verb) {
            case 'read':
                foreach (array_keys($fields) as $field) {
                    if (!in_array($field, [ 'a', 'b' ])) {
                        return false;
                    }
                }
    
                break;
    
            case 'create':
            case 'update':
            case 'delete':
            
                return false;
        }
    
        return null;
    }
    
# To be continued

### AUTHORS
Jonas Hallin

### License

Copyright (c) 2011 Jonas Hallin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.