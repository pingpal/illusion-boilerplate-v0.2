<?php

declare(ticks = 1);

namespace install;

use Illusion\Common\PM\Install;

file_exists(__DIR__ . '/composer.lock') || passthru(PHP_BINARY . ' composer.phar install');

include 'vendor/hallemaen/illusion-common/src/PM/Install.php';

(new Install(__DIR__, array_slice(@$argv ?: [], 1)))->begin();
