<?php

namespace Name\Space;

/**
 * # context api
 * # route get:/nsmf/(\d+) api Name.Space.Main.f id
 */
class Main {

    static function f($args, $in, $out) {

        $out->end(@$args['id']);
    }
}
